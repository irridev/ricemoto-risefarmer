import sample from 'lodash/sample';
import random from 'lodash/random';
import faker from 'faker';

export const generateMany = (count, callback) => Array(count).fill(0).map(callback);

const createFakeName = () => `${faker.name.firstName()} ${faker.name.lastName()}`;

export const generateFakeItem = type => {
  return {
    id: faker.random.uuid(),
    name: faker.commerce.productName(),
    description: faker.lorem.sentence(),
    quantity: random(10, 100),
    price: faker.commerce.price(),
    owner_id: faker.random.uuid(),
    type,
  };
};

const generateTransaction = () => {
  return {
    id: faker.random.uuid(),
    item_id: faker.random.uuid(),
    item_name: faker.commerce.productName(),
    quantity: random(10, 100),
    return_date: faker.date.future(),
  }
}

export const generateFakeTransactionAsSeller = () => {
  return {
    ...generateTransaction(),
    buyer_id: faker.random.uuid(),
  };
};

export const generateFakeTransactionAsBuyer = () => {
  return {
    ...generateTransaction(),
    seller_id: faker.random.uuid(),
    status: faker.random.arrayElement(['Pending', 'Rejected', 'Approved'])
  };
};

