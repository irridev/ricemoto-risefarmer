import React, { memo } from 'react'

import {
  StyleSheet,
  Image
} from 'react-native';

import {
  Text
} from 'react-native-ui-kitten';

import {
  Block,
} from 'shared'


function ProductItem(props) {

  const { type, name, description, price, quantity } = props;

  return (
    <Block
      flex={1}
      marginBottom={24}
      backgroundColor="#ffffff"
      elevation={3}
      paddingHorizontal={20}
      paddingVertical={20}
      style={styles.containerStyle}
    >
      <Block flex={1} row marginVertical={10}>
        <Block flex='disabled' middle>
          <Image style={styles.productImage} source={require('assets/images/search.png')} />
        </Block>
        <Block flex={1} marginLeft={24}>
          <Text style={styles.itemName}>{name}</Text>
          <Block flex="disabled" row>
            <Text style={styles.itemDesctiption}>{description}</Text>
          </Block>
          <Text>Price: P{price}</Text>
          <Text>Quantitiy: {quantity}</Text>
        </Block>
      </Block>
    </Block>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    borderColor: '#000000',
    // borderWidth: 1, 
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  productImage: {
    width: 50,
    height: 50,
    marginRight: 10,
    marginBottom: 10
  },
  itemName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  itemDesctiption: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 14,
    color: "#555555"
  },
  actionButton: {
    textDecorationLine: "underline",
    width: 50,
    textAlign: 'center'
  }
})

export default memo(ProductItem)