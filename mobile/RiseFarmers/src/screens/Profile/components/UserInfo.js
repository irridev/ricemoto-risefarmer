import React, { memo } from 'react'
import { StyleSheet } from 'react-native';

import { Text } from 'react-native-ui-kitten';

import {
  Block, Icon
} from 'shared'

function UserInfo(props) {

  const { name, mobileNumber } = props;

  return (
    <Block
      flex='disabled'
      marginBottom={24}
      backgroundColor="#ffffff"
      elevation={3}
      style={styles.containerStyle}
    >
      <Block flex={1} row middle padding={24}>
        {/* <Icon name='search' /> */}
        <Block flex={1} marginLeft={24} middle>
          <Text style={styles.itemName}>{name}</Text>
          <Text style={styles.itemDescription}>{mobileNumber}</Text>
        </Block>
      </Block>
    </Block>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    minHeight: 96,
    borderColor: '#000000',
    // borderWidth: 1, 
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  productImage: {
    width: 50,
    height: 50,
    marginRight: 10,
    marginBottom: 10
  },
  itemName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  itemDescription: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 14,
    color: "#555555"
  },
  actionButton: {
    textDecorationLine: "underline",
    width: 50,
    textAlign: 'center'
  }
})

export default memo(UserInfo);