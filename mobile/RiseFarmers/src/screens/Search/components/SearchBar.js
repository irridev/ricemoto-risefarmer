import React, { Fragment, memo, useState } from 'react';

import {
  Input,
  Block,
} from 'shared'

import {
  Image
} from 'react-native'

function Container(props) {
  const { onIconPress } = props;
  const [searchKey, setSearchKey] = useState("");

  const renderIcon = (style) => {
    return (
      <Image
        style={style}
        source={require('assets/images/search.png')}
      />
    );
  };

  return (
    <Block paddingHorizontal={24} flex="disabled" elevation={2} marginVertical={16}>
      <Input
        value={searchKey}
        icon={renderIcon}
        onIconPress={onIconPress}
        placeholder='Search product'
        onChangeText={setSearchKey}
        size="medium"
      />
    </Block>
  );
}

export default memo(Container);