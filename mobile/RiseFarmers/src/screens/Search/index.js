import React, { Fragment, memo, useState, useEffect } from 'react';

import {
  ScrollView,
  StyleSheet
} from 'react-native';

import {
  TabView,
  Tab,
  Spinner
} from 'react-native-ui-kitten'

import {
  HeaderBar,
  BackButton,
  TitleBar,
  Block
} from 'shared';

import {
  SearchBar,
  ProductItem
} from './components';

import { generateMany, generateFakeItem } from 'mockdata';

function Container() {
  const [forSaleItems, setForSaleItems] = useState(null);
  const [forRentItems, setforRentItems] = useState(null);
  const [selectedIndex, setSelectedIndex] = useState(0);

  useEffect(() => {
    const fakeSaleItems = generateMany(3, () => generateFakeItem('SALE'));
    const fakeRentItems = generateMany(3, () => generateFakeItem('RENT'));

    setTimeout(() => {
      setForSaleItems(fakeSaleItems);
      setforRentItems(fakeRentItems);
    }, 2000);
  
  }, []);

  const shouldLoadTabContent = index => {
    return index === selectedIndex;
  };

  const search = (searchKey, type) => {
    const fakeSaleItems = generateMany(3, () => generateFakeItem('SALE'));
    const fakeRentItems = generateMany(3, () => generateFakeItem('RENT'));
    setForSaleItems(fakeSaleItems);
    setforRentItems(fakeRentItems);
  }

  return (
    <Fragment>
      <HeaderBar
        leftControl={<BackButton />}
      />
      <TitleBar title="Products"/>
      <SearchBar onIconPress={search}/>
      <TabView style={styles.tabContainer}
        tabBarStyle={{ minHeight: 56 }}
        selectedIndex={selectedIndex}
        onSelect={setSelectedIndex}
        shouldLoadComponent={shouldLoadTabContent}
      >
        <Tab title='For Sale'>
          <ScrollView style={styles.scrollContainer} contentContainerStyle={{ paddingBottom: 56, paddingTop: 30 }}>
            {forSaleItems &&
              forSaleItems.map(({ id, type, name, description, price, quantity }) => (
                <ProductItem
                  key={id}
                  type={type}
                  name={name}
                  description={description}
                  price={price}
                  quantity={quantity}
                />
              ))
            }
            { !forSaleItems && 
              <Block flex={1} center middle>
                <Spinner />
              </Block> 
            }
          </ScrollView>
        </Tab>
        <Tab title='For Rent'>
          <ScrollView style={styles.scrollContainer} contentContainerStyle={{ paddingBottom: 56, paddingTop: 30 }}>
            {forRentItems && forRentItems.map(({ id, type, name, description, price, quantity }) => (
              <ProductItem
                key={id}
                type={type}
                name={name}
                description={description}
                price={price}
                quantity={quantity}
              />
            ))}
            {!forRentItems &&
              <Block flex={1} center middle>
                <Spinner />
              </Block>
            }
          </ScrollView>
        </Tab>
      </TabView>
    </Fragment>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    flex: 1,
  },
  scrollContainer: {
    paddingHorizontal: 20,
    paddingBottom: 20
  }
})

export default memo(Container);