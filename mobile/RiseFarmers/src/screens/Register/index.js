import React, { Fragment, memo, useState, useEffect, useRef } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import to from 'await-to-js';
import { Keyboard, Linking } from 'react-native';
import { WEBFORM_URL } from 'react-native-dotenv';

import {
  Text,
} from 'react-native-ui-kitten'

import {
  Block,
  HeaderBar,
  BackButton,
  Input,
  TitleBar,
  Button
} from 'shared';

import {
  UsersService, NavigationService
} from 'services';

function Container() {
  const [mobileNumber, setMobileNumber] = useState('9152533334');
  const [name, setName] = useState('Gio Peralta');
  const [password, setPassword] = useState('1234567');
  const [confirmPassword, setConfirmPassword] = useState('1234567');

  const [inputFocused, setInputFocused] = useState(false);

  const keyboardDidShowListener = useRef();
  const keyboardDidHideListener = useRef();

  const _keyboardDidShow = () => {
    setInputFocused(true)
  }

  const _keyboardDidHide = () => {
    setInputFocused(false)
  }

  useEffect(() => {
    keyboardDidShowListener.current = Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    keyboardDidHideListener.current = Keyboard.addListener('keyboardDidHide', _keyboardDidHide);
    return () => {
      keyboardDidShowListener.current.remove();
      keyboardDidHideListener.current.remove();
    }
  })

  const register = async () => {
    const data = {
      name: name,
      mobile_no: mobileNumber,
      password: password,
      confirm_password: confirmPassword,
    }

    const [error, response] = await to(UsersService.register(data));
    if (error) {

      if (typeof error.error === 'object' && error.error.message) {
        alert(error.error.message)
      }
      else {
        alert('Something went wrong!');
      }
    }
    else {
      setName('');
      setMobileNumber('');
      setPassword('');
      setConfirmPassword('');
    }

  };

  const switchToLogin = () => {
    NavigationService.navigate('Login');
  }

  const verifyNumber = async () => {
    await Linking.openURL(WEBFORM_URL);
  };

  return (
    <Fragment>
      <HeaderBar
        leftControl={<BackButton />}
      />
      <TitleBar title='Register'/>
      <Block
        center
        middle={inputFocused}
        marginTop={!inputFocused && 60}
        paddingHorizontal={16}
      >
        <Block flex={'disabled'} marginVertical={24}>
          <Text onPress={verifyNumber}>Click on this link to verify your mobile number.</Text>
        </Block>
        <Input
          value={name}
          placeholder='Name'
          size='small'
          marginBottom={6}
          onChangeText={setName}
        />
        <Input
          value={mobileNumber}
          placeholder='Mobile Number'
          size='small'
          marginBottom={6}
          onChangeText={setMobileNumber}
          keyboardType='phone-pad'
        />
        <Input
          value={password}
          placeholder='Password'
          size='small'
          marginBottom={6}
          onChangeText={setPassword}
          secureTextEntry
        />
        <Input
          value={confirmPassword}
          placeholder='Confirm Password'
          size='small'
          marginBottom={6}
          onChangeText={setConfirmPassword}
          secureTextEntry
        />
        <Button
          size='large'
          fullWidth
          marginBottom={6}
          onPress={register}
        >
          Register
        </Button>
        <Text onPress={switchToLogin}>Already have account? Login.</Text>
      </Block>
    </Fragment>
  );
}

export default memo(Container);