import React, { Fragment, memo, useState, useEffect, useRef } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { Keyboard } from 'react-native';
import to from 'await-to-js';

import {
  Text,
} from 'react-native-ui-kitten'

import {
  Block,
  HeaderBar,
  BackButton,
  Input,
  TitleBar,
  Button
} from 'shared';

import {
  AuthService, NavigationService
} from 'services';

function Container() {
  const [mobileNumber, setMobileNumber] = useState('9152533334');
  const [password, setPassword] = useState('1234567');
  const [inputFocused, setInputFocused] = useState(false);

  const keyboardDidShowListener = useRef()
  const keyboardDidHideListener = useRef()

  const _keyboardDidShow = () => {
    setInputFocused(true)
  }

  const _keyboardDidHide = () => {
    setInputFocused(false)
  }

  useEffect(() => {
    keyboardDidShowListener.current = Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    keyboardDidHideListener.current = Keyboard.addListener('keyboardDidHide', _keyboardDidHide);
    return () => {
      keyboardDidShowListener.current.remove();
      keyboardDidHideListener.current.remove();
    }
  })

  const login = async () => {
    const [ error, data ] = await to(AuthService.login({ mobile_no: mobileNumber, password }));
    if (error) {
      alert('Something went wrong!');
    }
    else {
      await AsyncStorage.setItem('token', data.data.token);

      setPassword('');
      setMobileNumber('');
      NavigationService.navigate('HomeTabs');
    }
  }

  const switchToRegister = () => {
    NavigationService.navigate('Register');
  }

  return (
    <Fragment>
      <HeaderBar
        leftControl={<BackButton />}
      />
      <TitleBar title='Login'/>
      <Block
        center
        middle={inputFocused}
        marginTop={!inputFocused && 60}
        paddingHorizontal={16}
      >
        <Input
          value={mobileNumber}
          placeholder='Mobile Number'
          size='small'
          marginBottom={6}
          onChangeText={setMobileNumber}
        />
        <Input
          value={password}
          placeholder='Password'
          size='small'
          marginBottom={6}
          onChangeText={setPassword}
          secureTextEntry
        />
        <Button
          size='large'
          fullWidth
          marginBottom={6}
          onPress={login}
        >
          Login
        </Button>
        <Text onPress={switchToRegister}>Don't have account yet? Register.</Text>
      </Block>
    </Fragment>
  );
}

export default memo(Container);