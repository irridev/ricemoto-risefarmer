import React, { Fragment, memo } from 'react'

import {
  StyleSheet,
  Image
} from 'react-native';

import {
  Text
} from 'react-native-ui-kitten';

import {
  Block,
} from 'shared'


function Transaction(props) {

  const { item_name, quantity, seller_id, buyer_id, return_date, status } = props;

  return (
    <Block
      flex={1}
      marginBottom={10}
      backgroundColor="#ffffff"
      elevation={2}
      paddingHorizontal={20}
      paddingVertical={20}
      style={styles.containerStyle}
    >
      <Block flex={1} row marginVertical={10}>
        <Block flex='disabled' middle>
          <Image style={styles.productImage} source={require('assets/images/search.png')} />
        </Block>
        <Block flex={1} marginLeft={24}>
          <Text style={styles.itemName}>Item ID: {item_name}</Text>
          <Text>Quantity: {quantity}</Text>
          <Text>{seller_id ? `Seller ID: ${seller_id}`: `Buyer ID: ${buyer_id}`}</Text>
          <Text>{return_date && `Return date: ${return_date}`}</Text>
          {seller_id ? 
            <Text style={styles.actionButton}>Status: {status}</Text> : 
            <Block flex="disabled" row>
              <Text style={styles.actionButton}>Confirm</Text>
              <Text style={styles.actionButton}>Reject</Text>
            </Block>
          }
        </Block>
      </Block>
    </Block>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    borderColor: '#000000', 
    // borderWidth: 1, 
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  productImage: {
    width: 50,
    height: 50,
    marginRight: 10,
    marginBottom: 10
  },
  itemName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  itemDesctiption: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 14,
    color: "#555555"
  },
  actionButton: {
    marginTop: 10,
    marginHorizontal: 20,
    textDecorationLine: "underline",
    textAlign: 'center'
  }
})

export default memo(Transaction)