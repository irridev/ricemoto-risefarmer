import React, { Fragment, memo, useState, useEffect } from 'react';

import {
  ScrollView,
  StyleSheet
} from 'react-native';

import {
  TabView,
  Tab,
} from 'react-native-ui-kitten'

import {
  HeaderBar,
  BackButton,
  TitleBar,
} from 'shared';

import {
  Transaction
} from './components'

import { generateMany, generateFakeTransactionAsSeller, generateFakeTransactionAsBuyer } from 'mockdata';

function Container() {
  const [sellerTransactions, setSellerTransactions] = useState([])
  const [buyerTransactions, setBuyerTransactions] = useState([])
  const [selectedIndex, setSelectedIndex] = useState(0)
  
  useEffect(() => {
    const fakeSellerTransactions = generateMany(10, () => generateFakeTransactionAsSeller());
    const fakeBuyerTransactions = generateMany(10, () => generateFakeTransactionAsBuyer());
    setSellerTransactions(fakeSellerTransactions);
    setBuyerTransactions(fakeBuyerTransactions);
  }, [])

  return (
    <Fragment>
      <HeaderBar/>
      <TitleBar title="Transactions"/>
      <TabView style={styles.tabContainer}
        selectedIndex={selectedIndex}
        onSelect={setSelectedIndex}>
        <Tab title='Your Requests'>
          <ScrollView style={styles.scrollContainer} contentContainerStyle={{ paddingBottom: 56 }}>
            {buyerTransactions.map(({ id, item_name, seller_id, price, quantity, return_date, status}) => (
              <Transaction
                key={id}
                item_name={item_name}
                quantity={quantity}
                price={price}
                return_date={return_date}
                seller_id={seller_id}
                status={status}
              />
            ))}
          </ScrollView>
        </Tab>
        <Tab title='Confirm Requests'>
          <ScrollView style={styles.scrollContainer} contentContainerStyle={{ paddingBottom: 56 }}>
            {sellerTransactions.map(({ id, item_name, price, buyer_id, quantity, return_date }) => (
              <Transaction
                key={id}
                item_name={item_name}
                quantity={quantity}
                price={price}
                buyer_id={buyer_id}
                return_date={return_date}
              />
            ))}
          </ScrollView>
        </Tab>
      </TabView>
    </Fragment>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    flex: 1,
    marginTop: 30,
    height: 40,
  },
  scrollContainer: {
    paddingHorizontal: 20,
    paddingBottom: 20
  }
})

export default memo(Container);