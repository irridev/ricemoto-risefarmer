import { light as lightTheme } from '@eva-design/eva';

export default theme = {
  ...lightTheme,

  'color-primary-100': '#E0FBDA',
  'color-primary-200': '#BDF8B6',
  'color-primary-300': '#8EEC8D',
  'color-primary-400': '#6CD975',
  'color-primary-500': '#40C057',
  'color-primary-600': '#2EA54F',
  'color-primary-700': '#208A47',
  'color-primary-800': '#146F3E',
  'color-primary-900': '#0C5C38',
  'color-success-100': '#ECFCD7',
  'color-success-200': '#D6FAconstAF',
  'color-success-300': '#B5F085',
  'color-success-400': '#94E163',
  'color-success-500': '#66CE35',
  'color-success-600': '#4AB126',
  'color-success-700': '#31941A',
  'color-success-800': '#1D7710',
  'color-success-900': '#0F620A',
  'color-info-100': '#D8E1FE',
  'color-info-200': '#B2C3FE',
  'color-info-300': '#8CA2FC',
  'color-info-400': '#6F88FA',
  'color-info-500': '#405EF7',
  'color-info-600': '#2E47D4',
  'color-info-700': '#2033B1',
  'color-info-800': '#14228F',
  'color-info-900': '#0C1676',
  'color-warning-100': '#FEFACB',
  'color-warning-200': '#FEF398',
  'color-warning-300': '#FCEA65',
  'color-warning-400': '#FAE13E',
  'color-warning-500': '#F7D200',
  'color-warning-600': '#D4B100',
  'color-warning-700': '#B19100',
  'color-warning-800': '#8F7300',
  'color-warning-900': '#765D00',
  'color-danger-100': '#FFE9D5',
  'color-danger-200': '#FFCCAC',
  'color-danger-300': '#FFAA82',
  'color-danger-400': '#FF8963',
  'color-danger-500': '#FF5230',
  'color-danger-600': '#DB3323',
  'color-danger-700': '#B71918',
  'color-danger-800': '#930F18',
  'color-danger-900': '#7A0919'

};