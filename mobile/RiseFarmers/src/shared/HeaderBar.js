import React, { memo } from 'react';
import {
  TopNavigation
} from 'react-native-ui-kitten';

import {
  withStyles
} from 'react-native-ui-kitten/theme';

function HeaderBar(props) {

  const {
    title, leftControl = null, rightControls = null,
    themedStyle
  } = props;

  return (
    <TopNavigation
      style={themedStyle.headerBar}
      title={title}
      titleStyle={themedStyle.titleStyle}
      alignment='start'
      leftControl={leftControl}
      rightControls={rightControls}
    />
  );

}

export default withStyles(memo(HeaderBar, () => true), theme => ({
  headerBar: {
    backgroundColor: 'transparent',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  titleStyle: {
    color: '#000000',
  },
  statusBar: {
    backgroundColor: theme['color-primary-100'],
  }
}));