import React, { memo } from 'react';
import { BottomNavigation } from 'react-native-ui-kitten';

import BottomTabBarTab from './BottomTabBarTab';

function BottomTabBar({ navigation }) {

  const onTabSelect = selectedIndex => {
    const { [selectedIndex]: selectedRoute } = navigation.state.routes;
    navigation.navigate(selectedRoute.routeName);
  };

  return (
    <BottomNavigation
      appearance='noIndicator'
      selectedIndex={navigation.state.index}
      onSelect={onTabSelect}
      style={{
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}
    >
      <BottomTabBarTab title='Search' iconName='search' />
      <BottomTabBarTab title='Transactions' iconName='list' />
      <BottomTabBarTab title='Profile' iconName='user' />
    </BottomNavigation>
  );

};

export default memo(BottomTabBar);