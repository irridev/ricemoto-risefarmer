import React, { memo } from 'react';
import { StyleSheet } from 'react-native';

import {
  Text
} from 'react-native';

import Block from './Block'


function TitleBar(props) {
  const { title } = props;

  return (
    <Block flex='disabled' paddingHorizontal={24} marginTop={24}>
      <Text style={styles.title}>{title}</Text>
    </Block>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    lineHeight: 30,
    color: "#000000",
    fontWeight: '700',
    fontFamily: 'opensans'
  }
})


export default memo(TitleBar)