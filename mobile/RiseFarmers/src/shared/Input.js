import React, { memo } from 'react';
import { Input as UKInput } from 'react-native-ui-kitten';
import { withStyles } from 'react-native-ui-kitten/theme';

function Input(props) {

  const {
    themedStyle,
    flex, row, center, left, middle, right, space,
    padding, paddingTop, paddingLeft, paddingRight, paddingBottom, paddingHorizontal, paddingVertical,
    margin, marginTop, marginLeft, marginRight, marginBottom, marginHorizontal, marginVertical,
    backgroundColor,
    style,
    ...otherProps
  } = props;

  const inputStyles = [
    style,
    flex && { flex },
    flex === 'disabled' && { flex: 0 },
    center && themedStyle.center,
    middle && themedStyle.middle,
    right && themedStyle.right,
    left && themedStyle.left,
    space && { justifyContent: `space-${space}` },
    backgroundColor && { backgroundColor },
    row && themedStyle.row,


    padding && { padding },
    paddingTop && { paddingTop },
    paddingLeft && { paddingLeft },
    paddingRight && { paddingRight },
    paddingBottom && { paddingBottom },
    paddingHorizontal && { paddingHorizontal },
    paddingVertical && { paddingVertical },

    margin && { margin },
    marginTop && { marginTop },
    marginLeft && { marginLeft },
    marginRight && { marginRight },
    marginBottom && { marginBottom },
    marginHorizontal && { marginHorizontal },
    marginVertical && { marginVertical },

  ];

  return (
    <UKInput
      style={inputStyles}
      textStyle={themedStyle.textStyle}
      {...otherProps} 
    />
  )

}

export default withStyles(memo(Input), () => ({
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center'
  },
  left: {
    justifyContent: 'flex-start',
  },
  middle: {
    justifyContent: 'center'
  },
  right: {
    justifyContent: 'flex-end'
  },
}));