import React, { memo } from 'react';

import HeaderBarButton from './HeaderBarButton';

function BackButton({ iconName = 'arrow-left' }) {

  const onPress = () => {
  };

  return (
    <HeaderBarButton
      iconName={iconName}
      onPress={onPress}
    />
  );

}

export default memo(BackButton, () => true);