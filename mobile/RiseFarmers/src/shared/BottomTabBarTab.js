import React, { memo } from 'react';
import { BottomNavigationTab } from 'react-native-ui-kitten';

import Icon from './Icon';

function BottomTabBarTab(props) {

  const { title, iconName, selected, ...restProps } = props;
  const renderIcon = () => (
    <Icon
      name={iconName}
      color={selected ? '#40C057' : '#8F9BB3'}
      size={28}
    />
  );

  const titleStyle = [
    {
      color: selected ? '#40C057' : '#8F9BB3',
    }
  ];

  return (
    <BottomNavigationTab
      title={title}
      titleStyle={titleStyle}
      icon={renderIcon}
      {...restProps}
    />
  );

};

export default memo(BottomTabBarTab);