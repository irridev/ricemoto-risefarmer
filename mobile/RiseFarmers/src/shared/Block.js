import React, { memo } from 'react';
import { View } from 'react-native';
import { withStyles } from 'react-native-ui-kitten/theme';

function Block(props) {

  const {
    themedStyle,
    flex, row, center, left, middle, right, space,
    padding, paddingTop, paddingLeft, paddingRight, paddingBottom, paddingHorizontal, paddingVertical,
    margin, marginTop, marginLeft, marginRight, marginBottom, marginHorizontal, marginVertical,
    backgroundColor, elevation, style,
    ...otherProps
  } = props;

  const blockStyles = [
    themedStyle.block,
    flex && { flex },
    flex === 'disabled' && { flex: 0 },
    center && themedStyle.center,
    middle && themedStyle.middle,
    right && themedStyle.right,
    left && themedStyle.left,
    space && { justifyContent: `space-${space}` },
    backgroundColor && { backgroundColor },
    row && themedStyle.row,


    padding && { padding },
    paddingTop && { paddingTop },
    paddingLeft && { paddingLeft },
    paddingRight && { paddingRight },
    paddingBottom && { paddingBottom },
    paddingHorizontal && { paddingHorizontal },
    paddingVertical && { paddingVertical },

    margin && { margin },
    marginTop && { marginTop },
    marginLeft && { marginLeft },
    marginRight && { marginRight },
    marginBottom && { marginBottom },
    marginHorizontal && { marginHorizontal },
    marginVertical && { marginVertical },

    elevation && { elevation },
    style,
  ];

  return (
    <View
      style={blockStyles}
      {...otherProps}
    />
  )

}

export default withStyles(memo(Block), () => ({
  block: {
    flex: 1,
    margin: 0,
    padding: 0
  },
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center'
  },
  left: {
    justifyContent: 'flex-start',
  },
  middle: {
    justifyContent: 'center'
  },
  right: {
    justifyContent: 'flex-end'
  },
}));