import React, { memo } from 'react';
import { Button as UKButton } from 'react-native-ui-kitten';
import { withStyles } from 'react-native-ui-kitten/theme';
import { responsiveHeight } from 'react-native-responsive-dimensions';

function Button(props) {

  const {
    themedStyle,
    flex, row, center, left, middle, right, space, fullWidth,
    padding, paddingTop, paddingLeft, paddingRight, paddingBottom, paddingHorizontal, paddingVertical,
    margin, marginTop, marginLeft, marginRight, marginBottom, marginHorizontal, marginVertical,
    backgroundColor,
    style,
    ...otherProps
  } = props;

  const buttonStyles = [
    style,
    flex && { flex },
    flex === 'disabled' && { flex: 0 },
    center && themedStyle.center,
    middle && themedStyle.middle,
    right && themedStyle.right,
    left && themedStyle.left,
    space && { justifyContent: `space-${space}` },
    backgroundColor && { backgroundColor },
    row && themedStyle.row,
    fullWidth && themedStyle.fullWidth,


    padding && { padding },
    paddingTop && { paddingTop },
    paddingLeft && { paddingLeft },
    paddingRight && { paddingRight },
    paddingBottom && { paddingBottom },
    paddingHorizontal && { paddingHorizontal },
    paddingVertical && { paddingVertical },

    margin && { margin },
    marginTop && { marginTop },
    marginLeft && { marginLeft },
    marginRight && { marginRight },
    marginBottom && { marginBottom },
    marginHorizontal && { marginHorizontal },
    marginVertical && { marginVertical },

  ];

  return (
    <UKButton
      style={buttonStyles}
      {...otherProps}
    />
  )

}

export default withStyles(memo(Button), () => ({
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center'
  },
  left: {
    justifyContent: 'flex-start',
  },
  middle: {
    justifyContent: 'center'
  },
  right: {
    justifyContent: 'flex-end'
  },
  fullWidth: {
    width: responsiveHeight(50)
  }
}));