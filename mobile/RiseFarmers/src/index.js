import React, { memo } from 'react';
import { useScreens } from 'react-native-screens';
import { mapping } from '@eva-design/eva';
import { ApplicationProvider } from 'react-native-ui-kitten';

import RootNavigator from 'navigation';
import { ModalService, NavigationService } from 'services'
import theme from 'constants/theme';
import { ModalContainer } from 'shared';

useScreens();

function App() {

  return (
    <ApplicationProvider
      mapping={mapping}
      theme={theme}
    >
      <RootNavigator ref={NavigationService.setTopLevelNavigator} />
      <ModalContainer ref={ModalService.setModalContainerRef} />
    </ApplicationProvider>
  );

}

export default memo(App);