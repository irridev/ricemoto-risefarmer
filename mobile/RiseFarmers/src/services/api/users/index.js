import base from '../base';

const service = {
  register({name, mobile_no, password, confirm_password}) {
    return base.post('/users/register', { name, mobile_no, password, confirm_password });
  },
};

export default service;