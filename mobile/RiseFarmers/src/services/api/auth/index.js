import base from '../base';

const service = {
  login({mobile_no, password}) {
    return base.post('/auth/login', { mobile_no, password });
  },
};

export default service;