class Modal {

  modalContainerRef;

  setModalContainerRef = ref => {
    this.modalContainerRef = ref;
  }

  showModal = (id, props) => {
    this.modalContainerRef.showModal(id, props);
  }

}

export default new Modal();