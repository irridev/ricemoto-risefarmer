import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import {
  Login,
  Register
} from 'screens';

import HomeTabs from './HomeTabs';

const RootNavigator = createSwitchNavigator({
  Login: Login,
  Register: Register,
  HomeTabs: HomeTabs,
}, {
    initialRouteName: 'Login'
});

export default createAppContainer(RootNavigator);