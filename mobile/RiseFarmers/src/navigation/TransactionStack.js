import { createStackNavigator } from 'react-navigation';

import {
  Transactions
} from 'screens';

const navigator = createStackNavigator({
  TransactionTabs: Transactions
}, {
  initialRouteName: 'TransactionTabs',
  defaultNavigationOptions: ({ navigation }) => {
    return {
      header: null,
    }
  }
});

navigator.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: navigation.state.index > 0 ? false : true,
  };
};

export default navigator;