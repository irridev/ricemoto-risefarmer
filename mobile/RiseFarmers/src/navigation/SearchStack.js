import { createStackNavigator } from 'react-navigation';

import {
  Search
} from 'screens';

const navigator = createStackNavigator({
  SearchProducts: Search 
}, {
  initialRouteName: 'SearchProducts',
  defaultNavigationOptions: ({ navigation }) => {
    return {
      header: null,
    }
  }
});

navigator.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: navigation.state.index > 0 ? false : true,
  };
};

export default navigator;