import { createStackNavigator } from 'react-navigation';

import {
  Profile
} from 'screens';

const navigator = createStackNavigator({
  UserProfile: Profile
}, {
    initialRouteName: 'UserProfile',
    defaultNavigationOptions: ({ navigation }) => {
      return {
        header: null,
      }
    }
  });

navigator.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: navigation.state.index > 0 ? false : true,
  };
};

export default navigator;