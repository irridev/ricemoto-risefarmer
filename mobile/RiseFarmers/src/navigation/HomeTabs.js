import { createBottomTabNavigator } from 'react-navigation';

import { BottomTabBar } from 'shared';

import SearchStack from './SearchStack';
import TransactionStack from './TransactionStack';
import ProfileStack from './ProfileStack';

const navigator = createBottomTabNavigator({
  Search: SearchStack,
  Transactions: TransactionStack,
  Profile: ProfileStack,
},
  {
    initialRouteName: 'Search',
    tabBarComponent: BottomTabBar,
    defaultNavigationOptions: ({ navigation }) => {
    },
    lazy: true,
  });

export default navigator;