![Logo of the project](./logo.sample.png)

# Rise Farmers
> Additional information or tag line

A brief description of your project, what it is used for.

## Installing / Getting started

Clone the repository.

```shell
git clone https://github.com/JoemaNequinto/rise-farmers.git
cd rise-farmers
sudo npm install
./scripts/mysql-db-create.sh <dbname> <dbuser> <dbpass>
```

The above commands installs the dependencies of the project.

## Developing

### Built With
| Feature                                                                                               |
|-------------------------------------------------------------------------------------------------------|
| Code linting using [ESLint](http://eslint.org/)                                                       |
| Automatic syntax formatting using [prettier](https://github.com/prettier/prettier)                    |
| Auto-restart server using [nodemon](https://nodemon.io/)                                              |
| HTTP access control using [cors](https://github.com/expressjs/cors)                                   |
| Authentication using [Passport.js](http://passportjs.org/) and [JSON Web Tokens](https://jwt.io/)     |
| Password hashing using [bcryptjs](https://www.npmjs.com/package/bcryptjs)                             |
| An easy-to-use multi SQL dialect ORM for Node.js [Sequelize](http://docs.sequelizejs.com)             |

### Prerequisites
What is needed to set up the dev environment. For instance, global dependencies or any other tools. include download links.


### Setting up Dev

To start developing with code linter.

```shell
npm run dev
```

### Building

If your project needs some additional steps for the developer to build the
project after some code changes, state them here. for example:

```shell
./configure
make
make install
```

Here again you should state what actually happens when the code above gets
executed.

### Deploying / Publishing
give instructions on how to build and release a new version
In case there's some step you have to take that publishes this project to a
server, this is the right time to state it.

```shell
packagemanager deploy your-project -s server.com -u username -p password
```

And again you'd need to tell what the previous code actually does.

## Versioning

We can maybe use [SemVer](http://semver.org/) for versioning. For the versions available, see the [link to tags on this repository](/tags).


## Configuration

Here you should write what are all of the configurations a user can enter when
using the project.

## Tests

Describe and show how to run the tests with code examples.
Explain what these tests test and why.

```shell
Give an example
```

## Style guide

Explain your code style and show how to check it.

## Api Reference

If the api is external, link to api documentation. If not describe your api including authentication methods as well as explaining all the endpoints with their required parameters.


## Database

Explaining what database (and version) has been used. Provide download links.
Documents your database design and schemas, relations etc...

## Licensing

State what the license is and how to find the text version of the license.
