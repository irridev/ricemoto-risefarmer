/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable no-undef */
import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import models from './models';

import logFactory from './helpers/logFactory';

const log = logFactory('passport'); // eslint-disable-line
const Authorized = models.authorized;
const User = models.user;

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
};

passport.use(
  new JwtStrategy(opts, async (jwt_payload, done) => {
    try {
      const authorized = await Authorized.findByPk(jwt_payload.jti);

      if (!authorized) {
        return done(null, false);
      }

      const user = await User.findByPk(jwt_payload.sub);

      if (!user) {
        return done(null, false);
      }

      return done(null, user);
    } catch (err) {
      return done(err, false);
    }
  }),
);
