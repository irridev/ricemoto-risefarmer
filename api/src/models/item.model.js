export default function(sequelize, DataTypes) {
  const item = sequelize.define(
    'item',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      name: {
        type: DataTypes.STRING,
      },
      description: {
        type: DataTypes.STRING,
      },
      quantity: {
        type: DataTypes.INTEGER(6),
      },
      price: {
        type: DataTypes.DECIMAL(13, 4),
      },
      for: {
        type: DataTypes.STRING(50),
      },
      status: {
        type: DataTypes.STRING(50),
      },
      owner_id: {
        type: DataTypes.UUID,
      },
    },
    {
      underscored: true,
      paranoid: true,
      freezeTableName: true,
    },
  );

  item.associate = models => {
    item.belongsTo(models.user, {
      foreignKey: 'owner_id',
      as: 'owner',
    });
  };

  item.searchables = ['name', 'description'];

  return item;
}
