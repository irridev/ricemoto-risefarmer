export default function(sequelize, DataTypes) {
  const transaction = sequelize.define(
    'transaction',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      item_id: {
        type: DataTypes.UUID,
      },
      quantity: {
        type: DataTypes.INTEGER(6),
      },
      return_date: {
        type: DataTypes.DATEONLY,
      },
      buyer_id: {
        type: DataTypes.UUID,
      },
      status: {
        type: DataTypes.STRING(50),
      },
    },
    {
      underscored: true,
      paranoid: true,
      freezeTableName: true,
    },
  );

  transaction.associate = models => {
    transaction.belongsTo(models.item, {
      foreignKey: 'item_id',
      as: 'item',
    });

    transaction.belongsTo(models.user, {
      foreignKey: 'buyer_id',
      as: 'buyer',
    });
  };

  return transaction;
}
