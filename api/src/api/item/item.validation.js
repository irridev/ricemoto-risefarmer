import Joi from 'joi';

export default {
  getAll: {},
  getItem: {
    params: {
      id: Joi.string().required(),
    },
  },
  postItem: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      quantity: Joi.number().required(),
      price: Joi.number().required(),
      for: Joi.string().required(),
    },
  },
  updateItem: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      quantity: Joi.number().required(),
      price: Joi.number().required(),
      for: Joi.string().required(),
    },
    params: {
      id: Joi.string().required(),
    },
  },
  removeItem: {
    params: {
      id: Joi.string().required(),
    },
  },
};
