/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "next" }] */
import debug from 'debug';
// import nodemailer from 'nodemailer';
// import uuidv4 from 'uuid';
import expressValidation from 'express-validation';
import Sequelize from 'sequelize';
import models from '../../models';
import { APISuccess, APIClientError } from '../../helpers/APIResponse';

const log = debug('user.controller'); // eslint-disable-line
const { Op } = Sequelize;
// const User = models.user;
const Item = models.item;

/**
 * @api {get} /items   GetAll
 * @apiName GetAll
 * @apiGroup Item
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object[]} data.data             Array of Item
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.name          Item name
 * @apiSuccess {String} data.data.description   Item description
 * @apiSuccess {Integer} data.data.quantity     Item quantity
 * @apiSuccess {Decimal} data.data.price        Item price
 * @apiSuccess {String="SALE","RENT"} data.data.for           Item for SALE or RENT
 * @apiSuccess {String} data.data.status        Item status
 * @apiSuccess {UUID} data.data.owner_id        Item owner id
 * @apiSuccess {Integer} data.total             Total count
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": [
 *          {
 *            "id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31",
 *            "name": "Rice Fertilizer"
 *            "description": "Rice Fertilizer",
 *            "quantity": "10",
 *            "price": 1500,
 *            "for": "SALE",
 *            "status": null,
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *          },
 *          {
 *            "id": "dc6a01e9-53c3-4c72-ac29-151bcfdcc35e",
 *            "name": "Rice Seedlings",
 *            "description": "Rice Seedlings",
 *            "quantity": 5,
 *            "price": 1200,
 *            "for": "RENT",
 *            "status": null,
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *          }
 *        ],
 *        "total": 10
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getAll = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 10;
    const offset = (page - 1) * limit;
    const { search } = req.query;

    const where = {};

    // Search Query
    if (search) {
      where[Op.or] = [];
      Item.searchables.forEach(field => {
        const condition = {};
        condition[field] = {
          [Op.like]: `%${search}%`,
        };
        where[Op.or].push(condition);
      });
    }

    const items = await Item.findAndCountAll({
      where,
      offset,
      limit,
    });

    const response = new APISuccess({
      data: items.rows,
      total: items.count,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {get} /items/:id     GetItem
 * @apiName GetItem
 * @apiGroup Item
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} id Item id
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object} data.data               Item data
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.name          Item name
 * @apiSuccess {String} data.data.description   Item description
 * @apiSuccess {Integer} data.data.quantity     Item quantity
 * @apiSuccess {Decimal} data.data.price        Item price
 * @apiSuccess {String="SALE","RENT"} data.data.for           Item for SALE or RENT
 * @apiSuccess {String} data.data.status        Item status
 * @apiSuccess {UUID} data.data.owner_id        Item owner id
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": {
 *            "id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31",
 *            "name": "Rice Fertilizer"
 *            "description": "Rice Fertilizer",
 *            "quantity": 10,
 *            "price": 1500,
 *            "for": "SALE",
 *            "status": null,
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *        },
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getItem = async (req, res, next) => {
  try {
    const item = await Item.findOne({
      where: { id: req.params.id },
    });

    const response = new APISuccess({ data: item });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {post} /items      PostItem
 * @apiName PostItem
 * @apiGroup Item
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {String} name            Item name
 * @apiParam {String} description     Item description
 * @apiParam {Integer} quantity       Item quantity
 * @apiParam {String="SALE","RENT"} for             Item for SALE or RENT
 * @apiParam {Decimal} price          Item price
 *
 * @apiSuccess {Number} status        Status Code of the response
 * @apiSuccess {String} statusText    Status Text of the response
 * @apiSuccess {Object} data          Data of the response
 * @apiSuccess {String} data.message  Message of the reponse
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "message": "Successfully posted an item for RENT."
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 400 BAD REQUEST
 *    {
 *      "name": "APIClientError",
 *      "status": 400,
 *      "statusText": "Bad Request",
 *      "error": {
 *        "message": "Invalid type. Allowed values are: SALE, RENT."
 *      }
 *    }
 */
export const postItem = async (req, res, next) => {
  try {
    const types = ['SALE', 'RENT'];

    if (!types.includes(req.body.for)) {
      throw new APIClientError({
        message: `Invalid type. Allowed values are: ${types.join(', ')}.`,
      });
    }

    const item = {
      name: req.body.name,
      description: req.body.description,
      quantity: req.body.quantity,
      price: req.body.price,
      for: req.body.for,
      owner_id: req.user.id,
    };

    await Item.create(item);

    const response = new APISuccess({
      message: `Successfully posted an item for ${item.for}.`,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {put} /items/:id      UpdateItem
 * @apiName UpdateItem
 * @apiGroup Item
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} id                Item UUID
 *
 * @apiParam {String} name            Item name
 * @apiParam {String} description     Item description
 * @apiParam {Integer} quantity       Item quantity
 * @apiParam {String="SALE","RENT"} for             Item for SALE or RENT
 * @apiParam {Decimal} price          Item price
 *
 * @apiSuccess {Number} status        Status Code of the response
 * @apiSuccess {String} statusText    Status Text of the response
 * @apiSuccess {Object} data          Data of the response
 * @apiSuccess {String} data.message  Message of the reponse
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "message": "Successfully updated an item for RENT."
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 400 BAD REQUEST
 *    {
 *      "name": "APIClientError",
 *      "status": 400,
 *      "statusText": "Bad Request",
 *      "error": {
 *        "message": "Invalid type. Allowed values are: SALE, RENT."
 *      }
 *    }
 */
export const updateItem = async (req, res, next) => {
  try {
    const types = ['SALE', 'RENT'];

    if (!types.includes(req.body.for)) {
      throw new APIClientError({
        message: `Invalid type. Allowed values are: ${types.join(', ')}.`,
      });
    }

    const item = await Item.findOne({ where: { id: req.params.id } });

    if (!item) {
      throw new APIClientError({
        message: 'Item not found.',
      });
    }

    if (item.owner_id !== req.user.id) {
      throw new APIClientError({
        message: 'You cannot edit this item.',
      });
    }

    const data = {
      name: req.body.name,
      description: req.body.description,
      quantity: req.body.quantity,
      price: req.body.price,
      for: req.body.for,
      owner_id: req.user.id,
    };

    await Item.update(data, { where: { id: req.params.id } });

    const response = new APISuccess({
      message: `Successfully updated an item for ${data.for}.`,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {delete} /items/:id     DeleteItem
 * @apiName DeleteItem
 * @apiGroup Item
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} id Item id
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Strinf} data.message            Message of the response
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": {
 *            "message": "Successfully deleted Rice Fertilizer."
 *        },
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const removeItem = async (req, res, next) => {
  try {
    const item = await Item.findOne({ where: { id: req.params.id } });

    if (!item) {
      throw new APIClientError({
        message: 'Item not found.',
      });
    }

    if (item.owner_id !== req.user.id) {
      throw new APIClientError({
        message: 'You cannot delete this item.',
      });
    }

    await Item.destroy({ where: { id: req.params.id } });

    const response = new APISuccess({
      message: `Successfully deleted ${item.name}.`,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};
