import express from 'express';
import passport from 'passport';
import debug from 'debug';
import validate from 'express-validation';
import * as controller from './item.controller';
import validation from './item.validation';
import wrapAsync from '../../helpers/wrapAsync';

const router = express.Router();
const passportJWT = passport.authenticate('jwt', {
  session: false,
  failWithError: true,
});
const log = debug('item.routes'); // eslint-disable-line

// GET
router.get('/items', passportJWT, wrapAsync(controller.getAll));
router.get(
  '/items/:id',
  passportJWT,
  validate(validation.getItem),
  wrapAsync(controller.getItem),
);

// POST
router.post(
  '/items',
  passportJWT,
  validate(validation.postItem),
  wrapAsync(controller.postItem),
);

// PUT
router.put(
  '/items/:id',
  passportJWT,
  validate(validation.updateItem),
  wrapAsync(controller.updateItem),
);

// DELETE
router.delete(
  '/items/:id',
  passportJWT,
  validate(validation.removeItem),
  wrapAsync(controller.removeItem),
);

export default router;
