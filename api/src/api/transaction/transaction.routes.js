import express from 'express';
import passport from 'passport';
import debug from 'debug';
import validate from 'express-validation';
import * as controller from './transaction.controller';
import validation from './transaction.validation';
import wrapAsync from '../../helpers/wrapAsync';

const router = express.Router();
const passportJWT = passport.authenticate('jwt', {
  session: false,
  failWithError: true,
});
const log = debug('item.routes'); // eslint-disable-line

// GET
router.get('/transactions', passportJWT, wrapAsync(controller.getAll));
router.get(
  '/transactions/buyer',
  passportJWT,
  validate(validation.getMyTransactionsAsBuyer),
  wrapAsync(controller.getMyTransactionsAsBuyer),
);
router.get(
  '/transactions/seller',
  passportJWT,
  validate(validation.getMyTransactionsAsSeller),
  wrapAsync(controller.getMyTransactionsAsSeller),
);
router.get(
  '/transactions/:id',
  passportJWT,
  validate(validation.getTransaction),
  wrapAsync(controller.getTransaction),
);

// POST
router.post(
  '/transactions',
  passportJWT,
  validate(validation.createTransaction),
  wrapAsync(controller.createTransaction),
);

// PUT
router.put(
  '/transactions/:id/:action',
  passportJWT,
  validate(validation.updateTransaction),
  wrapAsync(controller.updateTransaction),
);

// DELETE
router.delete(
  '/transactions/:id',
  passportJWT,
  validate(validation.removeTransaction),
  wrapAsync(controller.removeTransaction),
);

export default router;
