/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "next" }] */
import debug from 'debug';
// import nodemailer from 'nodemailer';
// import uuidv4 from 'uuid';
import expressValidation from 'express-validation';
// import Sequelize from 'sequelize';
import models from '../../models';
import { APISuccess, APIClientError } from '../../helpers/APIResponse';

import { isValidDate } from '../../helpers/utils';

const log = debug('user.controller'); // eslint-disable-line
// const { Op } = Sequelize;
// const User = models.user;
const Item = models.item;
const Transaction = models.transaction;

/**
 * @api {get} /transactions   GetAll
 * @apiName GetAll
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object[]} data.data             Array of Transaction
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.item_id       Transaction item
 * @apiSuccess {Integer} data.data.quantity     Transaction quantity
 * @apiSuccess {Date} data.data.return_date     Transaction return date
 * @apiSuccess {UUID} data.data.buyer_id        Transaction buyer id
 * @apiSuccess {String="PENDING","CONFIRMED","REJECTED"} data.data.status        Transaction status
 * @apiSuccess {Integer} data.total             Total count
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": [
 *          {
 *            "id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31",
 *            "item_id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31"
 *            "quantity": 10,
 *            "return_date": "2019-09-26",
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "PENDING",
 *          },
 *          {
 *            "id": "dc6a01e9-53c3-4c72-ac29-151bcfdcc35e",
 *            "item_id": "6f6cc500-8f7e-4d64-a7fb-b6edfb793733",
 *            "quantity": 5,
 *            "return_date": "2019-09-19",
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "CONFIRMED",
 *          }
 *        ],
 *        "total": 10
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getAll = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 10;
    const offset = (page - 1) * limit;

    const transactions = await Transaction.findAndCountAll({
      include: ['item'],
      offset,
      limit,
    });

    const response = new APISuccess({
      data: transactions.rows,
      total: transactions.count,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {get} /transactions/buyer   MyTransactionsAsBuyer
 * @apiName MyTransactionsAsBuyer
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object[]} data.data             Array of Transaction
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.item_id       Transaction item
 * @apiSuccess {Integer} data.data.quantity     Transaction quantity
 * @apiSuccess {Date} data.data.return_date     Transaction return date
 * @apiSuccess {UUID} data.data.buyer_id        Transaction buyer id
 * @apiSuccess {String="PENDING","CONFIRMED","REJECTED"} data.data.status        Transaction status
 * @apiSuccess {Integer} data.total             Total count
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": [
 *          {
 *            "id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31",
 *            "item_id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31"
 *            "quantity": 10,
 *            "return_date": "2019-09-26",
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "PENDING",
 *          },
 *          {
 *            "id": "dc6a01e9-53c3-4c72-ac29-151bcfdcc35e",
 *            "item_id": "6f6cc500-8f7e-4d64-a7fb-b6edfb793733",
 *            "quantity": 5,
 *            "return_date": "2019-09-19",
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "CONFIRMED",
 *          }
 *        ],
 *        "total": 10
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getMyTransactionsAsBuyer = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 10;
    const offset = (page - 1) * limit;

    const transactions = await Transaction.findAndCountAll({
      include: ['item'],
      where: { buyer_id: req.user.id },
      order: [['created_at', 'desc']],
      offset,
      limit,
    });

    const response = new APISuccess({
      data: transactions.rows,
      total: transactions.count,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {get} /transactions/seller   MyTransactionsAsSeller
 * @apiName MyTransactionsAsSeller
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object[]} data.data             Array of Transaction
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.item_id       Transaction item
 * @apiSuccess {Integer} data.data.quantity     Transaction quantity
 * @apiSuccess {Date} data.data.return_date     Transaction return date
 * @apiSuccess {UUID} data.data.buyer_id        Transaction buyer id
 * @apiSuccess {String="PENDING","CONFIRMED","REJECTED"} data.data.status        Transaction status
 * @apiSuccess {Integer} data.total             Total count
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": [
 *          {
 *            "id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31",
 *            "item_id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31"
 *            "quantity": 10,
 *            "return_date": "2019-09-26",
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "PENDING",
 *          },
 *          {
 *            "id": "dc6a01e9-53c3-4c72-ac29-151bcfdcc35e",
 *            "item_id": "6f6cc500-8f7e-4d64-a7fb-b6edfb793733",
 *            "quantity": 5,
 *            "return_date": "2019-09-19",
 *            "owner_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "CONFIRMED",
 *          }
 *        ],
 *        "total": 10
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getMyTransactionsAsSeller = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 10;
    const offset = (page - 1) * limit;

    const transactions = await Transaction.findAndCountAll({
      include: [
        {
          model: Item,
          as: 'item',
          where: { owner_id: req.user.id },
        },
      ],
      order: [['created_at', 'asc']],
      offset,
      limit,
    });

    const response = new APISuccess({
      data: transactions.rows,
      total: transactions.count,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {get} /transactions/:id     GetTransaction
 * @apiName GetTransaction
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} id Transaction id
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object} data.data               Transaction data
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {UUID} data.data.item_id         Transaction item
 * @apiSuccess {Integer} data.data.quantity     Transaction quantity
 * @apiSuccess {Date} data.data.return_date     Transaction return date
 * @apiSuccess {UUID} data.data.buyer_id        Transaction buyer
 * @apiSuccess {String="PENDING","CONFIRMED","REJECTED"} data.data.status        Transaction status
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": {
 *            "id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31",
 *            "item_id": "afbb835c-1b64-4f68-a3a6-b1fb5df26b31"
 *            "quantity": 10,
 *            "return_date": "2019-09-26",
 *            "buyer_id": "afa01169-cbc6-11e9-a962-d46d6d083a81",
 *            "status": "PENDING",
 *        },
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getTransaction = async (req, res, next) => {
  try {
    const transaction = await Transaction.findOne({
      where: { id: req.params.id },
    });

    const response = new APISuccess({ data: transaction });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {post} /transactions      CreateTransaction
 * @apiName CreateTransaction
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} item_id           Item UUID
 * @apiParam {Integer} quantity       Item quantity
 * @apiParam {Date} [return_date]     Item return date
 *
 * @apiSuccess {Number} status        Status Code of the response
 * @apiSuccess {String} statusText    Status Text of the response
 * @apiSuccess {Object} data          Data of the response
 * @apiSuccess {String} data.message  Message of the reponse
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "message": "Transaction created. Please wait for the owner's confirmation."
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 400 BAD REQUEST
 *    {
 *      "name": "APIClientError",
 *      "status": 400,
 *      "statusText": "Bad Request",
 *      "error": {
 *        "message": "Item not found."
 *      }
 *    }
 */
export const createTransaction = async (req, res, next) => {
  try {
    const item = await Item.findOne({ where: { id: req.body.item_id } });
    if (!item) {
      throw new APIClientError({
        message: 'Item not found.',
      });
    }

    if (item.owner_id === req.user.id) {
      throw new APIClientError({
        message: 'You cannot order your own items.',
      });
    }

    if (item.quantity < req.body.quantity) {
      throw new APIClientError({
        message: 'Insufficient supply.',
      });
    }

    if (item.for === 'RENT') {
      if (!req.body.return_date) {
        throw new APIClientError({
          message: 'Return date is missing.',
        });
      }
      if (!isValidDate(req.body.return_date)) {
        throw new APIClientError({
          message: 'Invalid return date. Must be in the format of YYYY-MM-DD.',
        });
      }
    }

    if (item.for === 'SALE' && req.body.return_date) {
      throw new APIClientError({
        message: 'Unnecessary return date.',
      });
    }

    const transaction = {
      item_id: req.body.item_id,
      quantity: req.body.quantity,
      return_date: req.body.return_date,
      buyer_id: req.user.id,
      status: 'PENDING',
    };

    await Transaction.create(transaction);

    const response = new APISuccess({
      message: `Transaction created. Please wait for the owner's confirmation.`,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {put} /transactions/:id/:action      UpdateTransaction
 * @apiName UpdateTransaction
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} id           Transaction UUID
 * @apiParam {String="CONFIRMED","REJECTED"} action       Action
 *
 * @apiSuccess {Number} status        Status Code of the response
 * @apiSuccess {String} statusText    Status Text of the response
 * @apiSuccess {Object} data          Data of the response
 * @apiSuccess {String} data.message  Message of the reponse
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "message": "Transaction is now confirmed."
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 400 BAD REQUEST
 *    {
 *      "name": "APIClientError",
 *      "status": 400,
 *      "statusText": "Bad Request",
 *      "error": {
 *        "message": "Transaction not found."
 *      }
 *    }
 */
export const updateTransaction = async (req, res, next) => {
  try {
    const actions = ['CONFIRMED', 'REJECTED'];

    if (!actions.includes(req.params.action)) {
      throw new APIClientError({
        message: `Invalid action. Allowed actions are: ${actions.join(', ')}.`,
      });
    }

    const transaction = await Transaction.findOne({
      include: ['item'],
      where: { id: req.params.id },
    });

    if (!transaction) {
      throw new APIClientError({
        message: 'Transaction not found.',
      });
    }

    if (transaction.item.owner_id !== req.user.id) {
      throw new APIClientError({
        message: 'You cannot update this transaction.',
      });
    }

    await Transaction.update(
      { status: req.params.action },
      { where: { id: req.params.id } },
    );

    const response = new APISuccess({
      message: `Transaction is now ${req.params.action}.`,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {delete} /transactions/:id     DeleteTransaction
 * @apiName DeleteTransaction
 * @apiGroup Transaction
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {UUID} id Transaction id
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Strinf} data.message            Message of the response
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": {
 *            "message": "Successfully deleted the transaction."
 *        },
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const removeTransaction = async (req, res, next) => {
  try {
    const transaction = await Transaction.findOne({
      where: { id: req.params.id },
    });

    if (!transaction) {
      throw new APIClientError({
        message: 'Transaction not found.',
      });
    }

    if (transaction.buyer_id !== req.user.id) {
      throw new APIClientError({
        message: 'You cannot delete this transaction.',
      });
    }

    await Transaction.destroy({ where: { id: req.params.id } });

    const response = new APISuccess({
      message: `Successfully deleted the transaction.`,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};
