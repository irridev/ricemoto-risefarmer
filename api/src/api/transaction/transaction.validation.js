import Joi from 'joi';

export default {
  getAll: {},
  getMyTransactionsAsBuyer: {},
  getMyTransactionsAsSeller: {},
  getTransaction: {
    params: {
      id: Joi.string().required(),
    },
  },
  createTransaction: {
    body: {
      item_id: Joi.string().required(),
      quantity: Joi.number().required(),
      return_date: Joi.string(),
    },
  },
  updateTransaction: {
    params: {
      id: Joi.string().required(),
      action: Joi.string().required(),
    },
  },
  removeTransaction: {
    params: {
      id: Joi.string().required(),
    },
  },
};
