import Joi from 'joi';

export default {
  getAll: {},
  getUser: {
    params: {
      mobile_no: Joi.string().required(),
    },
  },
  register: {
    body: {
      name: Joi.string().required(),
      mobile_no: Joi.string().required(),
      password: Joi.string().required(),
      confirm_password: Joi.string().required(),
    },
  },
};
