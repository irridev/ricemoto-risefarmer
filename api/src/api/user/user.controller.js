/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "next" }] */
import debug from 'debug';
// import nodemailer from 'nodemailer';
// import uuidv4 from 'uuid';
import expressValidation from 'express-validation';
// import Sequelize from 'sequelize';
import models from '../../models';
import { generateToken, getJTI } from '../../helpers/authentication';
import { APISuccess, APIClientError } from '../../helpers/APIResponse';

const log = debug('user.controller'); // eslint-disable-line
// const { Op } = Sequelize;
const User = models.user;
const Authorized = models.authorized;

/**
 * @api {get} /users   GetAll
 * @apiName GetAll
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object[]} data.data             Array of User
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.name          User name
 * @apiSuccess {String} data.data.password      User hashed password
 * @apiSuccess {String} data.data.mobile_no     User mobile number
 * @apiSuccess {String} data.data.access_token  User access token
 * @apiSuccess {String} data.data.latitude      User location latitude
 * @apiSuccess {String} data.data.longitude     User location longitude
 * @apiSuccess {String} data.data.map_url       User google map url location
 * @apiSuccess {String} data.data.status        User status
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": [
 *          {
 *            "id": "59b4d677076b8918a7841e73",
 *            "name": "Joema Nequinto"
 *            "password": "$2a$10$dxy4cTXzH1rNMeJ46erDaeF/TyRbjA/qjCvpuFeMip.lvqhV4tzTW",
 *            "mobile_no": "09951062561",
 *            "access_token": "UGGmKYNzYBV5wQkB5ECtzn41DIQ0j51GLDoxcqUE5cA"
 *            "latitude": "14.1725347",
 *            "longitude": "121.2496231",
 *            "map_url": "http://maps.google.com/maps?z=17&t=m&q=loc:14.1725347+121.2496231",
 *            "status": "REGISTERED",
 *          },
 *          {
 *            "id": "59b53be1e161e34bfba5c4b2",
 *            "name": "John Doe",
 *            "password": "$2a$10$Abv87/OZI4TAsZ5Nu5Nwd.fs/aMSXQqBZVWtV3CsqYp/O/zOnGscC",
 *            "mobile_no": "09951062561",
 *            "access_token": "64JhIJh7JqiUeM3-tyDnTu7uovqEu5ZgPe6TrxEypFk"
 *            "latitude": "14.1725347",
 *            "longitude": "121.2496231",
 *            "map_url": "http://maps.google.com/maps?z=17&t=m&q=loc:14.1725347+121.2496231",
 *            "status": "REGISTERED",
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getAll = async (req, res, next) => {
  try {
    const users = await User.findAll();

    const response = new APISuccess({
      data: users,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {get} /users/:mobile_no     GetUser
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Token
 *
 * @apiParam {String} mobile_no User mobile number
 *
 * @apiSuccess {Number} status                  Status Code of the response
 * @apiSuccess {String} statusText              Status Text of the response
 * @apiSuccess {Object} data                    Data of the response
 * @apiSuccess {Object} data.data               User data
 * @apiSuccess {UUID} data.data.id              UUID
 * @apiSuccess {String} data.data.name          User name
 * @apiSuccess {String} data.data.password      User hashed password
 * @apiSuccess {String} data.data.mobile_no     User mobile number
 * @apiSuccess {String} data.data.access_token  User access token
 * @apiSuccess {String} data.data.latitude      User location latitude
 * @apiSuccess {String} data.data.longitude     User location longitude
 * @apiSuccess {String} data.data.map_url       User google map url location
 * @apiSuccess {String} data.data.status        User status
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "data": {
 *            "id": "59b4d677076b8918a7841e73",
 *            "name": "Joema Nequinto"
 *            "password": "$2a$10$dxy4cTXzH1rNMeJ46erDaeF/TyRbjA/qjCvpuFeMip.lvqhV4tzTW",
 *            "mobile_no": "09951062561",
 *            "access_token": "UGGmKYNzYBV5wQkB5ECtzn41DIQ0j51GLDoxcqUE5cA"
 *            "latitude": "14.1725347",
 *            "longitude": "121.2496231",
 *            "map_url": "http://maps.google.com/maps?z=17&t=m&q=loc:14.1725347+121.2496231",
 *            "status": "REGISTERED",
 *        }
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 401 UNAUTHORIZED
 *    {
 *      "name": "APIClientError",
 *      "status": 401,
 *      "statusText": "Unauthorized",
 *      "error": {
 *        "message": "Passport.js authentication failed."
 *      }
 *    }
 */
export const getUser = async (req, res, next) => {
  try {
    const user = await User.findOne({
      where: { mobile_no: req.params.mobile_no },
    });

    const response = new APISuccess({ data: user });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

/**
 * @api {post} /users/register      Register
 * @apiName Register
 * @apiGroup User
 *
 * @apiParam {String} name User name
 * @apiParam {String} mobile_no User mobile number
 * @apiParam {String} password User password
 * @apiParam {String} confirm_password User confirm password
 *
 * @apiSuccess {Number} status      Status Code of the response
 * @apiSuccess {String} statusText  Status Text of the response
 * @apiSuccess {Object} data        Data of the response
 * @apiSuccess {String} data.token  JSON Web Token of the authenticated user
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "status": 200,
 *      "statusText": "OK",
 *      "data": {
 *        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
 *            .eyJzdWIiOiI1OWI0ZDY3NzA3NmI4OTE4YTc4NDFlNzMiLCJpYXQiOjE1MDcwMTY5NDcsImV4cCI6MTUwNzAyMDU0N30
 *            .VbhHJuYU8wfAP3VvYk80e3wskL5kzkqlDdrrcINjcSo"
 *      }
 *    }
 *
 * @apiError {String} name          Name of the error
 * @apiError {Number} status        Status Code of the response
 * @apiError {String} statusText    Status Text of the response
 * @apiError {String} error         Data of the error
 * @apiError {String} error.message Message of the error
 *
 * @apiErrorExample {json} Error-Response:
 *    HTTP/1.1 400 BAD REQUEST
 *    {
 *      "name": "APIClientError",
 *      "status": 400,
 *      "statusText": "Bad Request",
 *      "error": {
 *        "message": "Mobile number is already registered."
 *      }
 *    }
 */
export const register = async (req, res, next) => {
  try {
    // Check if password and confirmPassword are the same
    if (req.body.password !== req.body.confirm_password) {
      throw new APIClientError({
        message: 'Passwords do not match.',
      });
    }

    // Check if the mobile number is subscribed to SMS module
    const subscribedUser = await User.findOne({
      where: {
        mobile_no: req.body.mobile_no,
      },
    });

    // If no subscribed user
    if (!subscribedUser) {
      throw new APIClientError({
        message: 'Mobile number is not yet subscribed.',
      });
    }

    if (subscribedUser.status === 'REGISTERED') {
      throw new APIClientError({
        message: 'Mobile number is already registered.',
      });
    }

    subscribedUser.name = req.body.name;
    subscribedUser.password = req.body.password;
    subscribedUser.status = 'REGISTERED';

    await subscribedUser.save(subscribedUser);

    const token = generateToken({
      sub: subscribedUser.id,
    });

    const jti = getJTI(token);

    await Authorized.create({ id: jti });

    const response = new APISuccess({
      token,
    });
    return res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};
