import Joi from 'joi';

export default {
  login: {
    body: {
      mobile_no: Joi.string().required(),
      password: Joi.string().required(),
    },
  },
};
