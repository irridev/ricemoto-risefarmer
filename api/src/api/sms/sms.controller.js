/* eslint-disable camelcase */
/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "next" }] */
import debug from 'debug';
import rp from 'request-promise';
// import nodemailer from 'nodemailer';
// import uuidv4 from 'uuid';
import expressValidation from 'express-validation';
// import Sequelize from 'sequelize';
import models from '../../models';
// import { generateToken } from '../../helpers/authentication';
import { APISuccess, APIClientError } from '../../helpers/APIResponse';
import * as parser from '../../helpers/MsgParse';

const log = debug('user.controller'); // eslint-disable-line
// const { Op } = Sequelize;
const User = models.user;

export const receiveMessage = async (req, res) => {
  try {
    // const response = new APISuccess({
    //   data: req.body,
    // });
    const {
      message,
      senderAddress,
    } = req.body.inboundSMSMessageList.inboundSMSMessage[0];

    await parser.parse(message, senderAddress);
    return res.send({ message: 'Success' });
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

export const subscribe = async (req, res, next) => {
  try {
    let { access_token, subscriber_number } = req.query;
    const { code } = req.query;
    // if access_token does not exist, get them by using globelabs api
    if ((!access_token || !subscriber_number) && code) {
      const options = {
        method: 'POST',
        uri: `http://developer.globelabs.com.ph/oauth/access_token?app_id=RBGyCd9X4XH9dcML48TXAeH4EBMACe45&app_secret=8fa2f2594dca9bca5a3c0de6afbee42eda84fd56412cfc6ef46c763093c30281&code=${code}`,
        body: {},
        json: true,
      };

      const response = await rp(options);

      // eslint-disable-next-line prefer-destructuring
      access_token = response.access_token;
      // eslint-disable-next-line prefer-destructuring
      subscriber_number = response.subscriber_number;
    }

    // find user first if existing, then create
    const user = await User.findOrCreate({
      mobile_no: subscriber_number,
      access_token,
      where: {
        mobile_no: subscriber_number,
        access_token,
      },
    });

    // if name is null and app code is used
    if (user[0].name === null && code) {
      return res.status(409).send({ message: 'Please register' });
    }

    // if name is null and is sent through text, send a message to guide with SMS registration
    if (!code && !user[0].name) {
      const options = {
        method: 'POST',
        uri: `https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/4895/requests?access_token=${access_token}`,
        body: {
          senderAddress: 21584895,
          message:
            'Welcome to RiseFarmers. To register, just send REGISTER<SPACE><YOUR_NAME> and send to 21584895. E.g. REGISTER Juan Carlo M. Cruz',
          address: subscriber_number,
        },
        json: true,
      };
      await rp(options);
    }
    return res.send({ user });
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};

export const getUserLocation = async (req, res) => {
  try {
    const { access_token, address, requestedAccuracy } = req.query;
    const options = {
      uri: 'https://devapi.globelabs.com.ph/location/v1/queries/location',
      qs: {
        access_token,
        address,
        requestedAccuracy,
      },
      json: true,
    };
    const result = await rp(options);
    const response = new APISuccess({
      data: result,
    });

    res.send(response.jsonify());
  } catch (err) {
    if (
      !(
        err instanceof APIClientError ||
        err instanceof expressValidation.ValidationError
      )
    ) {
      throw new Error(err);
    } else {
      throw err;
    }
  }
};
