import express from 'express';
// import passport from 'passport';
import debug from 'debug';
// import validate from 'express-validation';
import * as controller from './sms.controller';
// import validation from './sms.validation';
import wrapAsync from '../../helpers/wrapAsync';

const router = express.Router();
// const passportJWT = passport.authenticate('jwt', {
//   session: false,
//   failWithError: true,
// });
const log = debug('sms.routes'); // eslint-disable-line

// GET
router.get('/sms/subscribe', wrapAsync(controller.subscribe));
router.get('/user/location', wrapAsync(controller.getUserLocation));

// POST
router.post(
  '/sms/receive-message',
  // validate(validation.register),
  wrapAsync(controller.receiveMessage),
);

// PUT

// DELETE

export default router;
