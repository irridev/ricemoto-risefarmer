import HTTPStatus from 'http-status';
import { red, green, blue } from 'chalk';
import passportMiddleWare from './passport';  // eslint-disable-line
import { APIClientError } from './helpers/APIResponse';
import wrapAsync from './helpers/wrapAsync';

// Routes
import authRoutes from './api/auth/auth.routes';
import userRoutes from './api/user/user.routes';
import smsRoutes from './api/sms/sms.routes';
import itemRoutes from './api/item/item.routes';
import transactionRoutes from './api/transaction/transaction.routes';

import logFactory from './helpers/logFactory';

const log = logFactory('routes');

export default app => {
  // logger
  app.use((req, res, next) => {
    log.debug(
      `${blue(new Date().toISOString())} [${red(req.method)}] ${green(
        req.url,
      )}`,
    );
    next();
  });

  // Insert routes below
  app.use('/api', authRoutes);
  app.use('/api', userRoutes);
  app.use('/api', smsRoutes);
  app.use('/api', itemRoutes);
  app.use('/api', transactionRoutes);

  // Handler for invalid routes
  app.all(
    '*',
    // eslint-disable-next-line
    wrapAsync(async (req, res, next) => {
      throw new APIClientError(
        {
          message: 'Invalid route.',
        },
        HTTPStatus.NOT_FOUND,
        HTTPStatus['404'],
      );
    }),
  );
};
