import logFactory from 'debug';

module.exports = function log(namespace) {
  const debug = logFactory(namespace);
  debug.log = (...args) => {
    console.log(...args); // eslint-disable-line
  };

  const error = logFactory(namespace);
  error.log = (...args) => {
    console.error(...args); // eslint-disable-line
  };

  return {
    debug,
    error,
  };
};
