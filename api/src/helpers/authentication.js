import jwt from 'jsonwebtoken';
import uuid from 'uuid/v4';

export function generateToken(payload) {
  return `Bearer ${jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXP,
    jwtid: uuid(),
  })}`;
}

export function getJTI(header) {
  try {
    const token = header.split(' ');
    const decoded = jwt.verify(token[1], process.env.JWT_SECRET);
    return decoded.jti;
  } catch (err) {
    return err;
  }
}
