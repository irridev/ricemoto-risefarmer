/* eslint-disable camelcase */
import rp from 'request-promise';
import Sequelize from 'sequelize';
import models from '../models';

const User = models.user;
const Item = models.item;

const { Op } = Sequelize;

const register = async (parsed_message, address) => {
  const user = await User.findOne({
    where: {
      mobile_no: address,
    },
  });
  if (user.name === null) {
    user.name = parsed_message.join(' ');
    await user.save();
    // send message that user is already registered
    const options = {
      method: 'POST',
      uri: `https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/4895/requests?access_token=${
        user.access_token
      }`,
      body: {
        senderAddress: 21584895,
        message: `Congratulations, ${
          user.name
        }. You have successfully registered to RiseFarmers. To list all available commands text TULONG and send to this number.`,
        address,
      },
      json: true,
    };
    await rp(options);
  } else {
    const options = {
      method: 'POST',
      uri: `https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/4895/requests?access_token=${
        user.access_token
      }`,
      body: {
        senderAddress: 21584895,
        message:
          'Sorry, this number has already been registered. To list all available commands text TULONG and send to this number.',
        address,
      },
      json: true,
    };
    await rp(options);
  }
};

const help = async address => {
  const user = await User.findOne({
    where: {
      mobile_no: address,
    },
  });
  const options = {
    method: 'POST',
    uri: `https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/4895/requests?access_token=${
      user.access_token
    }`,
    body: {
      senderAddress: 21584895,
      message: `List of Commands (text the keyword and send to 21584895):
        TULONG - ililista lahat ng posibleng command sa app na ito
        BILI<SPACE><item_na_bibilihin> - ilista ang pinakamalapit at pinakamurang nagbebenta ng iyong kailangan
        HIRAM<SPACE><item_na_bibilihin> - ilista ang pinakamalapit at pinakamurang nagpapahiram ng iyong kailangan
        PILI<SPACE><item_code><SPACE><ilan><SPACE><kailan_ibabalik> - pumili mula sa nakalistang produkto gamit ang item code, bilang ng nais hiramin/bilihin at kung kailan ito ibabalik
        SIGURADO - i-send ito kapag sigurado na sa napiling produkto`,
      address,
    },
    json: true,
  };
  await rp(options);
};

const search_for_sale = async (parsed_message, address) => {
  console.log(address);
  const where = {
    for: 'SALE',
  };
  where[Op.or] = [];
  Item.searchables.forEach(field => {
    const condition = {};
    condition[field] = {
      [Op.like]: `%${parsed_message.join(' ')}%`,
    };
    where[Op.or].push(condition);
  });
  const items = await Item.findAndCountAll({
    where,
    offset: 0,
    limit: 10,
  });
  console.log(items.row);
  console.log(items.count);
};

export const parse = async (message, address) => {
  const [mode, ...parsed_message] = message.split(' ');
  const number = address.split('+63')[1];

  switch (mode) {
    case 'REGISTER':
      register(parsed_message, number);
      break;
    case 'BILI':
      search_for_sale(parsed_message, number);
      break;
    case 'TULONG':
      help(number);
      break;
    case 'HIRAM':
      break;
    case 'PILI':
      break;
    case 'SIGURADO':
      break;
    default:
      break;
  }
};

export const something = () => {};
