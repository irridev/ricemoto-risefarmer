import moment from 'moment';

export function isValidDate(string) {
  return moment(string, 'YYYY-MM-DD', true).isValid();
}

export function clone(obj) {
  return JSON.parse(JSON.stringify(obj));
}
