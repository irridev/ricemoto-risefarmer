/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
import fs from 'fs';
import uuid from 'uuid/v4';
import moment from 'moment';
import * as items from './items.json';

const NL = `\n`;
const TAB = `\t`;
const created_at = moment().format('YYYY-MM-DD hh:mm:ss');

function toSQL(json, tableName) {
  const columns = Object.keys(json[0]);
  let data =
    `${NL}` +
    `INSERT INTO ${tableName}` +
    `${NL}` +
    `${TAB}` +
    `(${columns.join()})` +
    `${NL}` +
    `VALUES` +
    `${NL}` +
    `${TAB}`;

  json.forEach(p => {
    const temp = Object.values(p).map(e => {
      if (typeof e === 'string') e = `"${e}"`;
      return e;
    });
    data += `(${temp.join()}),${NL}${TAB}`;
  });

  data = data.substring(0, data.length - 3);
  data += `;${NL}`;

  return data;
}

function writeFile(filename, data) {
  fs.writeFile(`${__dirname}/${filename}`, data, err => {
    if (err) throw err;
    // eslint-disable-next-line no-console
    else console.log(`${filename} DONE!`);
  });
}

async function itemSQL() {
  const user = {
    id: uuid(),
    name: 'Juan Dela Cruz',
    password: '$2a$10$d2gi4dvYg6PXM3kLJT.65Oxzxt3wG2VHCbvDFyLhhK08HMDdHZuAO',
    mobile_no: '9951062561',
    access_token: 'UGGmKYNzYBV5wQkB5ECtzn41DIQ0j51GLDoxcqUE5cA',
    status: 'REGISTERED',
    created_at,
    updated_at: created_at,
  };
  const data = [];
  await Promise.all(
    Object.values(items).map(async obj => {
      const item = obj;
      item.id = uuid();
      item.description = obj.name;
      item.owner_id = user.id;
      item.created_at = created_at;
      item.updated_at = created_at;
      data.push(item);
      return obj;
    }),
  );

  data.pop();

  let string = ``;

  string += `SET autocommit=0;`;
  string += toSQL([user], 'user');
  string += toSQL(data, 'item');
  string += 'COMMIT;';

  return string;
}

async function start() {
  const data = await itemSQL();
  writeFile('items.sql', data);
}

start();
